package ch.teko.olten.android.swissairportfinder.model

object SwissAirports {

    private val airportMap = mapOf(
            "Zürich" to "ZRH",
            "Samedan" to "SMV",
            "St. Gallen" to "ACH",
            "Sion" to "SIR",
            "Lugano" to "LUG",
            "Genf" to "GVA",
            "Buochs" to "BXO",
            "Bern" to "BRN",
            "Basel" to "BSL"
    )

    fun getAirportName(ort:String) : String {
        return airportMap.getOrDefault(ort, "Sorry, in dieser Ortschaft kenne ich keinen Flughafen!")
    }

}