package ch.teko.olten.android.swissairportfinder

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ch.teko.olten.android.swissairportfinder.model.SwissAirports
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        searchBtn.setOnClickListener{
            val airportName = SwissAirports.getAirportName(locationTxt.text.toString())
            airportTxt.setText(airportName)
        }
    }
}
